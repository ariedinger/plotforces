#!/usr/bin/env python3

### --------------------------------------------------------------------------------------------
### USAGE
### --------------------------------------------------------------------------------------------
# python3 plotForces.py path/to/forces.dat

### --------------------------------------------------------------------------------------------

### --------------------------------------------------------------------------------------------
### USER LIBRARIES
### --------------------------------------------------------------------------------------------
import matplotlib.pyplot as plt
import numpy as np
import re
import sys

### --------------------------------------------------------------------------------------------

### --------------------------------------------------------------------------------------------
### DATA
### --------------------------------------------------------------------------------------------
# Open the file with the data
file = open(sys.argv[1],"r")

# Read all the lines in the forces.dat file and store them as a list
strFile = file.readlines()

# Close the data file - this is very important, if you don't do this the program will overwrite the file
file.close()

# Delete the first three lines - a.k.a the header
for i in range(3):
    del strFile[0]

# Create an empty array of length equal to the length of the data file
numbers = np.empty((len(strFile),13))

# Pattern to extract numbers from the file
pattern = r'-?\d+(?:\.\d+)?(?:[eE][-+]?\d+)?'

# Extract all the numbers from the data file and store them in a numpy matrix
for i in range(len(strFile)):
    numbers[i] = re.findall(pattern, strFile[i])

# Create arrays for time and forces with all the numbers from columns
t = numbers[:, 0]
fxPressure = numbers[:, 1]
fyPressure = numbers[:, 2]
fzPressure = numbers[:, 3]
fxViscous  = numbers[:, 4]
fyViscous  = numbers[:, 5]
fzViscous  = numbers[:, 6]

### --------------------------------------------------------------------------------------------

### --------------------------------------------------------------------------------------------
### PLOT
### --------------------------------------------------------------------------------------------
# Plot Pressure Force in the Z direction
plt.subplot(2, 3, 1)
plt.plot(t,fxPressure)
plt.title(r"$f_{x}$ Pressure")
plt.grid(True)

# Plot Pressure Force in the Y direction
plt.subplot(2, 3, 2)
plt.plot(t,fyPressure)
plt.title(r"$f_{y}$ Pressure")
plt.grid(True)

# Plot Pressure Force in the Z direction
plt.subplot(2, 3, 3)
plt.plot(t,fzPressure)
plt.title(r"$f_{z}$ Pressure")
plt.grid(True)

# Plot Viscous Force in the Y direction
plt.subplot(2, 3, 4)
plt.plot(t,fxViscous)
plt.title(r"$f_{x}$ Viscous")
plt.grid(True)

# Plot Viscous Force in the Y direction
plt.subplot(2, 3, 5)
plt.plot(t,fyViscous)
plt.title(r"$f_{y}$ Viscous")
plt.grid(True)

# Plot Viscous Force in the Z direction
plt.subplot(2, 3, 6)
plt.plot(t,fzViscous)
plt.title(r"$f_{z}$ Viscous")
plt.grid(True)

# Render plot
plt.suptitle(r"FORCES PLOT")
plt.grid(True)
plt.show()

### --------------------------------------------------------------------------------------------
